# Guidelines para fornecimento de resources gráficos

## Design e exportação

Os designs devem ser feitos com base no tamanho de um iPhone de 4" (iPhone 5/5S/5C), com 320 pontos de largura e 578 pontos de altura, pois é um formato obrigatório e que escala bem para todos os outros maiores, onde os elementos são os mesmos, apenas com mais espaço disponível, mas exportado em diferentes densidades (pontos são o tamanho lógico; a densidade do ecrã especifica qual a quantidade de pixeis por ponto; dobro da densidade indica que um ponto ocupa 2x2 pixeis, triplo da densidade ocupa 3x3 pixeis).

O export dos resources deve ser na densidade original (equivalente a largura de ecrã de 320px), o dobro da densidade (largura de 640px) e o triplo da densidade (largura de 960px).

Ou seja, um elemento que ocupe 100 pontos de largura deve ser exportado com 100px, 200px e 300px.

Larguras de ecrãs:

- 320 pontos - iPhone e Androids pequenos
- 360 pontos - Androids normais
- (375 pontos - iPhone 6)
- \> 600 pontos - Tablets Android
- 768/1024 pontos - iPad

Designs vectoriais são o mais adequado para este tipo de trabalho. Elementos exportados devem ser o mais possível alinhados à grelha de pixeis para não terem um aspecto desfocado.

As versões mais recentes do Photoshop têm uma funcionalidade para ajudar a exportar slices de vários tamanhos: [https://helpx.adobe.com/photoshop/using/generate-assets-layers.html](Generate image assets from layers).

Mesmo que não exista um design para iPad, tablets Android têm que ser minimamente suportados, embora normalmente se mantenha o design de telefone com alguns ajustes.

## Icon da aplicação

Os icons têm que ser quadrados e podem ser apresentados com as seguintes dimensões:

- App Store: 1024
- iPhone: 180, 120
- iPad: 152, 76
- Google Play: 512  
- Android: 192, 144, 120, 96, 72, 48

Não é necessário fornecer cada tamanho, apenas garantir que o icon é perfeitamente visível em todos eles.

Os icons para iOS NÃO devem ter os cantos arredondados nem transparência, é o sistema que trata disso. Por outro lado, em Android, o icon pode ter a forma que se quiser, sendo que este pode simplesmente ser um quadrado com cantos arredondados ou um círculo.

É ainda importante que o icon seja distinguível tanto em fundos claros como em fundos escuros.

## Particularidades de Elementos

### Elementos que mudam de estado

Se um elemento tem um estado “seleccionado”, este deve ser incluído, ou então deve ser indicada uma forma de modificar o elemento que se adeque a este estado. 

É também boa política providenciar o estado “premido” de elementos interactivos. Este estado normalmente dá uma cor mais escura ao elemento (mas não tão forte como a do estado “seleccionado”, caso o elemento tenha este estado).

### Elementos redimensionáveis

Os designs devem ter em conta que os elementos podem ter que suportar diferentes tamanhos, seja pelas várias dimensões de ecrã ou pelos diferentes tamanhos do seu conteúdo (por exemplo botões com textos de diferentes tamanhos).

Estes elementos redimensionáveis podem ser feitos considerando a utilização de técnicas que mantenham as partes exteriores da imagem fixas e estiquem apenas o meio da imagem. Para isso é necessário definir a quantidade de pixeis a fixar em cada lado da imagem. (ver `resizableImageWithCapInsets` no iOS e `9patch` no Android).

### Elementos que ocupem espaços semelhantes

Conjuntos de icons que podem ocupar locais comuns, como tabs, menus ou icons de elementos de listas devem poder ser alinhados da mesma forma, independentemente do seu desenho. Para isso devem ser gerados com canvas de tamanho igual, utilizando margens para alinhar cada desenho no mesmo canvas.

Por exemplo, icons que devem ser mostrados ao lado de itens numa tabela devem ter todos o mesmo canvas, por exemplo 50x50, independentemente do desenho ocupar apenas 50x10 ou 10x50.

### Cor dinâmica

Quando é suposto um elemento mudar de cor de forma dinâmica, existem várias formas possíveis de o fazer:

- Elemento é desenhado em código  
  Válido para elementos simples como círculos, rectângulos, linhas ou uma composição destes. Serve também criar fundos para outros elementos, como um circulo de uma cor com um icon de outra cor em cima.

- Elemento é uma imagem com uma única cor e transparência
  Quando o elemento é apenas uma cor e transparência, é fácil mudar essa cor em código, mantendo a transparência. Desta forma a cor original é irrelevante, apenas convém que não seja branco por ser difícil de distinguir.

- Elemento é uma imagem com várias cores fixas e uma que muda
  Caso seja necessário um elemento com estas características, e não seja simples fazer a separação entre elementos de cor fixa e de cor variável, pode-se deixar transparente a cor que deve mudar, colocando depois um fundo com a cor desejada.

- Elementos utilizando composição
  É possível fazer composições de imagens utilizando sobreposições “Porter-Duff”, como mais ou menos explicado [aqui](http://www.piwai.info/transparent-jpegs-done-right/).

É ainda possível utilizar as técnicas assim para compor um único elemento, como criar um icon com uma cor dinâmica e colocá-lo por cima de um circulo com outra cor dinâmica fazendo de fundo.


## Plataformas

Deve-se ter em conta a essência de cada plataforma. O design pode ser semelhante em cada uma, mas os padrões de cada devem ser aproveitadas, pois os utilizadores estão à espera deles, tornando a curva de aprendizagem menor.

Barras de navegação, *tabbars*, alertas, sidebars, etc devem poder ser o mais standard possível em cada plataforma, utilizando as respectivas técnicas de personalização. Re-implementar padrões por causa de pequenas diferenças pode causar uma degradação na experiência noutras partes desse elemento, devido a inconsistências com o resto do sistema. Regra que se pode e deve quebrar se existir um grande beneficio para o utilizador.
No entanto, convém balancear a utilização de funcionalidades de uma plataforma para que ambas as versões da aplicação não sejam demasiado dispares.

Devem igualmente ser utilizada iconografia adequada a cada plataforma. Existem alguns icons que são diferentes em Android e iOS, como o botão de partilhar ou o icon de localização geográfica. A aplicação deve utilizar icons que sejam identificáveis com os da respectiva plataforma.



## Animações

Se existem elementos ou ecrãs em que faça sentido existirem animações, estas devem ser especificadas no design.

## Tipografia

Devem ser utilizadas fontes nos formatos OpenType ou TrueType compatíveis. As fontes escolhidas devem ser adequadas a ecrãs de baixa densidade e devem ser testadas pelo menos em Android, em devices com pouca densidade de pixeis, pois algumas fontes tornam-se difíceis de ler ou ficam diferentes do pretendido. Especial atenção aos textos corridos e densos, onde o tamanho de letra normalmente não é tão grande como em títulos.

Deve ser especificado no design que fontes usar em que elementos, e em que tamanho. Não é possível transpor o tamanho definido no design directamente para a implementação, mas podem ser mapeados numa aproximação.

A ter ainda em conta que as plataformas permitem mudar o tamanho relativo do texto através das definições de acessibilidade do sistema.


## Mockups

É aconselhável criar um mockup interactivo do design, pegando em cada imagem de ecrã e criando links nos elementos interactivos que mostram o ecrã correspondente. Existem várias formas de fazer isto, a mais fácil é utilizando a webapp [Marvel](https://marvelapp.com), que permite a sincronização de imagens de ecrãs via dropbox e possibilita a criação de links entre screenshots, bem como algumas animações entre si. Estes mockups interactivos tornam muito mais fácil a compreensão do fluxo da aplicação.

## Links úteis

* **[The iOS Design Guidelines](http://iosdesign.ivomynttinen.com)**
* **[androiddesign.tips](http://www.androiddesign.tips)**
* [Photoshop: Generate image assets from layers](https://helpx.adobe.com/photoshop/using/generate-assets-layers.html)
* [Designer’s Guide to DPI](http://sebastien-gabriel.com/designers-guide-to-dpi/home)
* [Tips for exporting assets for iOS & Android design](http://unitid.nl/2012/07/10-free-tips-to-create-perfect-visual-assets-for-ios-and-android-tablet-and-mobile/)
* [Android Design](https://developer.android.com/design/index.html)

---
v2 • 2015/02/20